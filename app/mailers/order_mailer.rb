# encoding: utf-8
class OrderMailer < ActionMailer::Base

  default_url_options[:host] = "http://rocket-web.ru/"

  default :from => "order@rocket-web.ru"

  def order_cofirm(order)
    @order = order
    mail(:to => "levunova@mail.ru, levunova@yandex.ru, maxstbn@yandex.ru", :subject => "Поступил новый заказ на сайте karibuni.ru")
  end

  def order_cofirm_user(order)
    @order = order
    mail(:to => @order.email , :subject => "Заказ на сайте karibuni.ru")
  end

end